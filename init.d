#!/sbin/openrc-run

start() {
	ebegin "Starting Colormap monitoring Tmux session"
	start-stop-daemon --start --background --make-pidfile --pidfile /run/monitor.pid --exec ${SCRIPT_PATH}
	eend $?
}

stop() {
	ebegin "Stopping Colormap monitoring tmux session"
	pkill -SIGINT 'cm$'
	while [ $(pgrep -a 'cm$'|egrep python|wc -l) -gt 1 ] ; do
		sleep 1
	done
	rm /run/monitor.pid
	eend $?
}
