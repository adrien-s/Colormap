#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# vim: ts=4 sts=4 sw=4 noet foldlevel=0

"""Basic terminal gestion system"""

from sys import stdout

##### Variables #####
colIndex = {'front': 7, 'back': 0}
terminalSize = {'w': 80, 'h': 25}

##### Exceptions #####
class OutOfBoundError(Exception):
	pass

##### Functions #####
def init():
	"""Initialises terminal size and clears the screen"""
	global terminalSize
	
	from os import get_terminal_size

	terminalSize['w'], terminalSize['h'] = get_terminal_size()
	stdout.write("\x1B[2J\x1B[1;1H")

def color(frontColor = None, backColor = None):
	"""Sets front and/or back colors in 256-color palette mode from [R, G, B] triplet with 0≤[R, G, B]≤1
	frontColor: [red, green, blue] triplet. Color are in bounds 0 ≤ x ≤ 1
	backColor: [red, green, blue] triplet (see frontColor)"""
	global colIndex

	if frontColor != None:
		for idx, i in enumerate(frontColor):
			if i<0 or i>1:
				raise OutOfBoundError("Front color {0} component must be in range 0 ≤ x ≤ 1".format(["red", "green", "blue"][idx]))

		colIndex['front'] = 16 + int(frontColor[0]*5)*36 + int(frontColor[1]*5)*6 + int(frontColor[2]*5)
		stdout.write("\x1B[38;5;{0}m".format(colIndex['front']))

	if backColor != None:
		for idx, i in enumerate(backColor):
			if i<0 or i>1:
				raise OutOfBoundError("back color {0} component must be in range 0 ≤ x ≤ 1".format(["red", "green", "blue"][idx]))

		colIndex['back'] = 16 + int(backColor[0]*5)*36 + int(backColor[1]*5)*6 + int(backColor[2]*5)
		stdout.write("\x1B[48;5;{0}m".format(colIndex['back']))

def byteToGradient(byte):
	"""Takes a integer value and turns it into a hue color triplet.
	0 is black, 255 is red. Gradients go from black to blue to cyan to green to yellow to red.
	byte: Integer in the range 0 ≤ x ≤ 255
	returns a color triplet (see frontColor and backColor in function color())"""
	if byte<0 or byte>255:
		raise OutOfBoundError("byte must be in the range 0 ≤ byte ≤ 255")
	
	B = (byte<128) * (1-abs((byte/64)-1))
	G = (byte>=64 and byte<192) * (1 - abs(((byte-64)/64)-1))
	R = (byte>=128) * (1 - abs(((byte-128)/64)-1))
		
	return [R, G, B]

def move(position=[1, 1]):
	"""Move cursor to given position
	position: [x, y] duet"""
	if position[0]<1 or position[0]>terminalSize['w']:
		raise OutOfBoundError("X position must be in range 1 ≤ x ≤ {0}".format(terminalSize['w']))
	if position[1]<1 or position[1]>terminalSize['h']:
		raise OutOfBoundError("Y position must be in range 1 ≤ x ≤ {0}".format(terminalSize['h']))
	
	stdout.write("\x1B[{1};{0}H".format(position[1], position[0]))

def put(string, position=None, frontColor=None, backColor=None):
	"""Put string on screen, with optional colors and positions.
	string: The text to put (Unicode UTF-8 format for Python 3)
	position: optional. See move() function
	frontColor, backColor: optional. See color() function."""
	if position != None:
		move(position)

	color(frontColor, backColor)
	stdout.write(string)

def refresh():
	"""Refreshees the screen (simply putting text won't normally visually display it)"""
	stdout.flush()
