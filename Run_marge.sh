#!/usr/bin/env bash
WIDTH=273
HEIGHT=75

cd "$(dirname "${0}")"

if [ $(tmux list-sessions | grep '^monitor:' | wc -l) -gt 0 ]; then
	tmux kill-session -t monitor
fi

function ns() {
	title=$1
	shift
	tmux new -ds monitor -n "$title" -x $WIDTH -y $HEIGHT -- "${@}"
}
function nw() {
	sleep 10
	title=$1
	shift
	tmux neww -t monitor -n "$title" -- "${@}"
}
function sw() {
	monpane=$1
	shift
	tmux splitw -h -t monitor:$monpane -- "${@}"
}
function swb() {
	monpane=$1
	shift
	tmux splitw -v -l 3 -t monitor:$monpane -- "${@}"
}
function bkp() {
	[ -f "$1" ] && echo "--readbackup=$1" || echo "--backupfile=$1"
}

ns 'Temp: CPU/GPU'	cm $(bkp .cputemp) -m25 -M68 -n'CPU Temp' -u°C -p900 -l96 -F/sys/class/hwmon/hwmon0/temp1_input -c1E3 -C60																			#0.0
sw 0.0				cm $(bkp .gputemp) -m25 -M65 -n'GPU Temp' -u°C -p900 -l96 -S'nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits' -c1 -C60										#0.1
# sw 0.0				cm $(bkp .gputemp) -m25 -M95 -n'GPU Temp' -u°C -p900 -l96 -F/sys/class/hwmon/hwmon0/temp1_input -c1E3 -C60										#0.1

nw 'Temp: NB/SW'	cm $(bkp .nbtemp) -m25 -M80 -nNorthbridge -u°C -p900 -l96 -F/sys/class/hwmon/hwmon0/temp4_input -c1E3 -C60																			#1.0
sw 1.0				cm $(bkp .sbtemp) -m25 -M65 -nSouthbridge -u°C -p900 -l96 -F/sys/class/hwmon/hwmon0/temp3_input -c1E3 -C60																			#1.1

nw 'eth0'			cm $(bkp .eth0tx) -m0 -M122 -n'eth0 tx' -u'KiB/s' -p900 -l96 -F/sys/class/net/eth0/statistics/tx_bytes -d -c1024 -C60																#2.0
sw 2.0				cm $(bkp .eth0rx) -m0 -M122 -n'eth0 rx' -u'KiB/s' -p900 -l96 -F/sys/class/net/eth0/statistics/rx_bytes -d -c1024 -C60																#2.1

nw 'CPU Freq'		cm $(bkp .freq) -m1.6 -M4 -n'CPU Freq' -uGHz -p900 -l96 -S'bc -l<<<"($(cat /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq|tr "\n" "+"|sed "s/+$//"))/12"' -c1E6 -C60	#3.0
nw 'Load'			cm $(bkp .load) -a -m0 -M6 -nLoad -p900 -l96 -p900 -l96 -F/proc/loadavg -C60 -s0																									#4.0

nw 'sda'			cm $(bkp .sdar) -m0 -M10 -n'sda(r)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C60 -s2																					#5.0
sw 5.0				cm $(bkp .sdaw) -m0 -M10 -n'sda(w)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C60 -s6																					#5.1

nw 'Fan'			cm $(bkp .fan) -m.6 -M2.38 -n'CPU Fan' -u'kRPM' -p900 -l96 -F/sys/class/hwmon/hwmon0/fan1_input -c1E3 -C60																			#6.0

pgrep 'cm$' > /run/monitor.pid

tmux selectw -t monitor:0
