#!/usr/bin/env bash
WIDTH=273
HEIGHT=75

cd "$(dirname "${0}")"

if [ $(tmux list-sessions | grep '^monitor:' | wc -l) -gt 0 ]; then
	tmux kill-session -t monitor
fi

function ns() {
	title=$1
	shift
	tmux new -ds monitor -n "$title" -x $WIDTH -y $HEIGHT -- "${@}"
}
function nw() {
	sleep 30
	title=$1
	shift
	tmux neww -t monitor -n "$title" -- "${@}"
}
function sw() {
	monpane=$1
	shift
	tmux splitw -h -t monitor:$monpane -- "${@}"
}
function swb() {
	monpane=$1
	shift
	tmux splitw -v -l 4 -t monitor:$monpane -- "${@}"
}
function bkp() {
	[ -f "$1" ] && echo "--readbackup=$1" || echo "--backupfile=$1"
}

ns 'Temp: CPU'		/root/.local/bin/cm $(bkp .temp0) -m30 -M50 -n'CPU 0' -u°C -p900 -l96 -F/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp2_input -c1E3 -C5										#0.0
sw 0.0				/root/.local/bin/cm $(bkp .temp1) -m30 -M50 -n'CPU 1' -u°C -p900 -l96 -F/sys/devices/platform/coretemp.0/hwmon/hwmon0/temp3_input -c1E3 -C5										#0.1

nw 'eth0'			/root/.local/bin/cm $(bkp .eth0tx) -m0 -M122 -n'eth0 tx' -u'KiB/s' -l96 -p900 -F/sys/class/net/eth0/statistics/tx_bytes -d -c1024 -C5											#1.0
sw 1.0				/root/.local/bin/cm $(bkp .eth0rx) -m0 -M122 -n'eth0 rx' -u'KiB/s' -l96 -p900 -F/sys/class/net/eth0/statistics/rx_bytes -d -c1024 -C5											#1.1
nw 'tap0'			/root/.local/bin/cm $(bkp .tap0tx) -m0 -M122 -n'tap0 tx' -u'KiB/s' -l96 -p900 -F/sys/class/net/tap0/statistics/tx_bytes -d -c1024 -C5											#2.0
sw 2.0				/root/.local/bin/cm $(bkp .tap0rx) -m0 -M122 -n'tap0 rx' -u'KiB/s' -l96 -p900 -F/sys/class/net/tap0/statistics/rx_bytes -d -c1024 -C5											#2.1

nw 'CPU Freq'		/root/.local/bin/cm $(bkp .freq) -m0.798 -M1.862 -n'CPU Freq' -uGHz -l96 -p900 -S'bc -l<<<"($(cat /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq|tr "\n" "+"|sed "s/+$//"))/4"' -c1E6 -C5	#3.0
nw 'Load'			/root/.local/bin/cm $(bkp .load) -a -m0 -M2 -nLoad -p900 -l96 -F/proc/loadavg -C5 -s0																							#4.0

nw 'sda'			/root/.local/bin/cm $(bkp .sdar) -m0 -M10 -n'sda(r)' -u'MiB/s' -l96 -p900 -F/sys/block/sda/stat -d -c2048 -C5 -s2																#5.0
sw 5.0				/root/.local/bin/cm $(bkp .sdaw) -m0 -M10 -n'sda(w)' -u'MiB/s' -l96 -p900 -F/sys/block/sda/stat -d -c2048 -C5 -s6																#5.1

touch home.ipconn eqs.httpconn loult.httpconn dsc.disconnect
nw 'xdsl|eqs'	/root/.local/bin/cm $(bkp .homeip) -m0 -M100 -n'xDSL Connectivity' -u% -l96 -p900 -Fhome.ipconn -C60 -c0.01																			#6.0
sw 6.0			/root/.local/bin/cm $(bkp .eqshttp) -m0 -M100 -n'EQS Connectivity' -u% -l96 -p900 -Feqs.httpcon -C60 -c0.01																			#6.1
swb 6.0			'while true;do (ping -i1 -c3 -W1 109.190.244.9>/dev/null 2>&1 && echo 1 || echo 0)>home.ipconn;sleep 1m;done'																		#6.2
swb 6.2			'while true;do (curl -sI -m10 --connect-timeout 10 https://equestria.social/@Electron|egrep "200 OK"|wc -l)>eqs.httpcon;sleep 1m;done'												#6.3

nw 'loult'		/root/.local/bin/cm $(bkp .loulthttp) -m0 -M100 -n'Lou.lt connectivity' -u% -l96 -p900 -Floult.httpconn -C60 -c0.01																	#7.0
sw 7.0			/root/.local/bin/cm $(bkp .dsc) -m0 -M5 -n'Discord disconnects' -u'disconnect/day' -l120 -p86400 -Fdsc.disconnect -C86400 -d														#7.1
swb 7.0			'while true;do (curl -sI -m10 --connect-timeout 10 https://lou.lt/@Electron|head -n 1|egrep "^HTTP/\S+ (200|304)"|wc -l)>loult.httpcon;sleep 1m;done'								#7.2
swb 7.2			'while true;do grep "Chatroom closed by server" /var/lib/znc/.znc/moddata/log/arthur33/lisa/\#general/*.log|wc -l>dsc.disconnect;sleep 24h;done'									#7.3

tmux selectw -t monitor:0
