#!/usr/bin/env bash

rm -f /tmp/$$.old.lst /tmp/$$.new.lst
touch /tmp/$$.old.lst
url="${url-"http://vk-blood-world.forumactif.org/forum"}"

while true; do
	curl -m 60 -s "${url}"|grep -oP "Utilisateurs enregistrés.*?<br />"|grep -v "Aucun"|sed 's/^.*: //;s/\(, \)\?<a[^>]*>//g;s/<\/\?\(span\|strong\)[^>]*>//g;s/<br \/>//;s/<\/a>/\n/g'|sort|grep -v '^$' > /tmp/$$.new.lst
	diff -u1 /tmp/$$.old.lst /tmp/$$.new.lst|grep -v "^---\|^+++\|^@"|cut -c2-|sed "s/^/$(date +'%F %T') /g"
	cat /tmp/$$.new.lst > /tmp/$$.old.lst
	sleep 30
done | ./presence.py -i
