#!/usr/bin/env bash
WIDTH=273
HEIGHT=75

cd "$(dirname "${0}")"

if [ $(tmux list-sessions | grep '^monitor:' | wc -l) -gt 0 ]; then
	tmux kill-session -t monitor
fi

function ns() {
	title=$1
	shift
	tmux new -ds monitor -n "$title" -x $WIDTH -y $HEIGHT -- "${@}"
}
function nw() {
	title=$1
	sleep 2
	shift
	tmux neww -t monitor -n "$title" -- "${@}"
}
function sw() {
	monpane=$1
	shift
	tmux splitw -h -t monitor:$monpane -- "${@}"
}
function bkp() {
	f="$1"
	[ -f "$f"] && echo "--readbackup=$f" || echo "--backupfile=$f"
}

ns 'Temp: CPU'		cm $(bkp .cputemp) -m25 -M90 -p900 -l96 -n'CPU Temp' -u°C -F/sys/class/hwmon/hwmon0/temp1_input -c1E3 -C5																				#0.0

nw 'Batterie'		cm $(bkp .battcap) -m0 -M4.5 -p900 -l96 -nCharge -uAh -F/sys/class/power_supply/BAT1/charge_now -c1E6 -C5																				#1.0
sw 1.0				cm $(bkp .battcurrent) -m0 -M3 -p900 -l96 -nCourant -uA -F/sys/class/power_supply/BAT1/current_now -c1E6 -C5																			#1.1


nw 'eth0'			cm $(bkp .eth0tx) -m0 -M122 -n'eth0 tx' -u'KiB/s' -p900 -l96 -F/sys/class/net/eth0/statistics/tx_bytes -d -c1024 -C5																	#2.0
sw 2.0				cm $(bkp .eth0rx) -m0 -M122 -n'eth0 rx' -u'KiB/s' -p900 -l96 -F/sys/class/net/eth0/statistics/rx_bytes -d -c1024 -C5																	#2.1
nw 'wlan0'			cm $(bkp .wlan0tx) -m0 -M122 -n'wlan0 tx' -u'KiB/s' -p900 -l96 -F/sys/class/net/wlan0/statistics/tx_bytes -d -c1024 -C5																#3.0
sw 3.0				cm $(bkp .wlan0rx) -m0 -M122 -n'wlan0 rx' -u'KiB/s' -p900 -l96 -F/sys/class/net/wlan0/statistics/rx_bytes -d -c1024 -C5																#3.1

nw 'CPU Freq'		cm $(bkp .cpufreq) -m1.2 -M2.5 -n'CPU Freq' -p900 -l96 -uGHz -S'bc -l<<<"($(cat /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_cur_freq|tr "\n" "+"|sed "s/+$//"))/4"' -c1E6 -C5	#4.0
nw 'Load'			cm $(bkp .load) -a -m0 -M2 -nLoad -p900 -l96 -F/proc/loadavg -C5 -s0																										#5.0

nw 'sda'			cm $(bkp .sdar) -m0 -M10 -n'sda(r)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C5 -s2																						#6.0
sw 6.0				cm $(bkp .sdaw) -m0 -M10 -n'sda(w)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C5 -s6																						#6.1

pgrep 'cm$' > /run/monitor.pid

tmux selectw -t monitor:0
