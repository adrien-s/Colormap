#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from sys import argv, stdin, stdout

# itemNames = ["a", "b", "c"]
# switches=[(0, "00:00:00"), (1, "00:00:01"), (0, "00:00:02"), (2, "00:00:03")]
# itemStates={i:False for i in itemNames}

itemNames = []
switches = []
itemStates = {}

def printHeader(tsLen):
    print("\x1B[s\x1B[1H", end="")
    for i, name in enumerate(itemNames):
        print(" "*(1+tsLen) + ("│ "*i) + "\x1B[" + ["0;37", "1;30"][i%2] + "m" + name + "\x1B[0;37m\x1B[0K")
    print("\x1B[u", end="")

def switchItem(itemName, timestamp, add=True):
    global switches, itemNames, itemStates

    if itemNames.count(itemName)<1:
        itemNames.append(itemName)
        itemStates = {i:False for i in itemNames}
        runAllSwitches()

    if add:
        switches.append((itemNames.index(itemName), timestamp))

    itemStates[itemName] = not itemStates[itemName]
    print("\x1B[0;37m" + timestamp + " " + " ".join(["\x1B[" + ["0;37", "1;30"][num%2] + "m" + "│█▀▄"[itemStates[i]+2*(i==itemName)] for num, i in enumerate(itemNames)]))
    
def runAllSwitches():
    print("\x1B[2J\x1B[%dH" % len(itemNames))
    t = ""
    for nameIdx, timestamp in switches:
        switchItem(itemNames[nameIdx], timestamp, False)
        t = timestamp
    printHeader(len(t))

def loadFromFile(fn):
    f = open(fn, "r")
    print("\x1B[2J\x1B[1H")
    lines = [i[:-1] for i in f.readlines()]

    for line in lines:
        a = line.split()
        ts_date, ts_hour, name = a[0], a[1], " ".join(a[2:])
        switchItem(name, ts_date+" "+ts_hour)

def runFromStdin():
    print("\x1B[2J\x1B[1H")
    while True:
        a = stdin.readline()[:-1].split()
        ts_date, ts_hour, name = a[0], a[1], " ".join(a[2:])
        switchItem(name, ts_date+" "+ts_hour)

if __name__ == "__main__":
    if len(argv) > 1:
        if argv[1] == "-f":
            loadFromFile(argv[2])
        if argv[1] == "-i":
            runFromStdin()
