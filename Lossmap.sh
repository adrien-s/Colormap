#!/usr/bin/env bash

##### Parameters #####
target="homer"
logFile="${target}_loss.log"
intensity="100"
scaleMax="100"

playOnly=false
currentHour=$(date +%H)
pingMapColors=(16 52 88 124 160 196 202 208 214 220 226 190 154 118 82 46 47 48 49 50 51 45 39 33 27 21 20 19 18 17)
delay="$(bc<<<"scale=6;a=20/${intensity};if (a<10^-6) {10^-6} else {a}"|tr "." ",")"

clear
while [ "$1" != "" ]; do
	case "$1" in
		"-p"|"--play")		playOnly=true;;
		"-t"|"--target")	target="$2";
							logFile="${target}_loss.log";
							shift;;
		"-l"|"--logfile")	logFile="$2"; shift;;
		"-m"|"--maximum")	scaleMax="$2"; shift;;
		"-i"|"--intensity")	intensity="$2";
							delay="$(bc<<<"scale=6;a=20/${intensity};if (a<10^-6) {10^-6} else {a}"|tr "." ",")"
							shift;;
		"-h"|"--help")		echo -e "$0 [-h|--help] [-i|--intensity <integer>] [-l|--logfile <path>] [-t|--target <IP Address / DNS name>] [-p|--play] [-m|--max <float>]
-h, --help\tThis help screen
-t, --target\tThe target to ping to
-l, --logfile\tPath to te file to save ping loss to
-i, --intensity\tNumber of measures to do in a 20 second time span (more is better, but too many will enlarge measure time and make holes in the graph)
-m, --max\tMaximum percentage to show"
							exit 0;;
	esac
	shift
done

##### Scale #####
printf "\x1B[1mScale (%%) : \x1B[0m "
for i in {0..29}; do
	printf "\x1B[38;5;${pingMapColors[$i]};48;5;${pingMapColors[$i]}m▓▓"
done
printf "\x1B[0m (${target})\n\x1B[15G"
for i in {0..6}; do
	printf "\x1B[$((15 + 10*${i}))G│"
done
printf "\n"
for i in {0..6}; do
	printf "\x1B[$((15 + 10*${i}))G%s" "$(bc<<<"scale=1;${scaleMax} - (${i}*${scaleMax})/6")"
done
printf "\x1B[0m\n\n"
printf "    00        05        10        15        20        25        30        35        40        45        50        55\n    │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . ."

##### Replay log #####
tsToggle=0
if [ -f "${logFile}" ]; then
	ts=$(date +%s -d "$(head -n 1 "${logFile}"|cut -d" " -f1)")
	tmpHour=$(date +%H -d "@${ts}")
	currentHour="--"

	while [ ${ts} -lt $(date +%s) ]; do
		pingData=$(grep "$(date +%FT%T -d "@${ts}")" "${logFile}"|cut -d" " -f2)
		tmpHour=$(date +%H -d "@${ts}")
		if [ "${tmpHour}" != "${currentHour}" ]; then
			currentHour="${tmpHour}"
			echo ""
			if [ "${currentHour}" == "00" ]; then
				echo -e "\x1B[0m-- $(date +%A,\ %B\ %d\ %Y -d "@${ts}") --"
			fi
			printf "\x1B[1G\x1B[0m${currentHour}h │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . ."
		fi
		printf "\x1B[1G\x1B[0m${currentHour}h "
		printf "\x1B[$((5 + ($(date +%-S -d "@${ts}")/30) + 2*$(date +%-M -d "@${ts}")))G\x1B[1;38;5;${pingMapColors[${pingData}]};48;5;${pingMapColors[${pingData}]}m▓\x1B[5G"
		let ts+=30
	done
fi

if $playOnly; then
	echo -e "\x1B[0m"
	exit 0
fi

##### Main Loop #####
while true; do
	while [ "$(($(date +%-S)%30))" != "0" ]; do
		sleep 0.5;
	done
	curTime=$(date +%s)
	tmpHour=$(date +%H -d "@${curTime}")

	if [ "${tmpHour}" != "${currentHour}" ]; then
		currentHour="${tmpHour}"
		printf "\n"
		if [ "${currentHour}" == "00" ]; then
			printf "\x1B[0m-- $(date +%A,\ %B\ %d\ %Y) --\n"
		fi
		printf "\x1B[1G\x1B[0m${currentHour}h │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . . │ . . . . | . . . ."
	fi

	printf "\x1B[1G\x1B[0m${currentHour}h "
	
	printf "\x1B[$((5 + ($(date +%-S -d "@${curTime}")/30) + 2*$(date +%-M -d "@${curTime}")))G\x1B[90;100m░\x1B[5G"
	pingData=$(bc<<<"scale=3;a=${scaleMax}-((${intensity}-$((ping -i ${delay} -W 1 -c ${intensity} ${target})|grep -o "[0-9]* received"|cut -d" " -f 1))*100 / ${intensity});if (a<0) {0} else if (a>${scaleMax}) {29} else {((29*a)/${scaleMax})}"|cut -d. -f1)
	printf "\x1B[$((5 + ($(date +%-S -d "@${curTime}")/30) + 2*$(date +%-M -d "@${curTime}")))G\x1B[1;38;5;${pingMapColors[${pingData}]};48;5;${pingMapColors[${pingData}]}m▓\x1B[5G"
	
	echo "$(date +%FT%T -d "@${curTime}") ${pingData}" >> "${logFile}";
done
