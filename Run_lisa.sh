#!/usr/bin/env bash
WIDTH=273
HEIGHT=75

cd "$(dirname "${0}")"

if [ $(tmux list-sessions | grep '^monitor:' | wc -l) -gt 0 ]; then
	tmux kill-session -t monitor
fi

function ns() {
	title=$1
	shift
	tmux new -ds monitor -n "$title" -x $WIDTH -y $HEIGHT -- "${@}"
}
function nw() {
	sleep 2
	title=$1
	shift
	tmux neww -t monitor -n "$title" -- "${@}"
}
function sw() {
	monpane=$1
	shift
	tmux splitw -h -t monitor:$monpane -- "${@}"
}
function swb() {
	monpane=$1
	shift
	tmux splitw -v -l 3 -t monitor:$monpane -- "${@}"
}
function bkp() {
	[ -f "$1" ] && echo "--readbackup=$1" || echo "--backupfile=$1"
}

ns 'Temp'		cm $(bkp .temp) -m25 -M70 -n'CPU Temp' -u°C -p900 -l96 -F/sys/class/hwmon/hwmon0/temp2_input -c1E3 -C5																						#0.0

nw 'eth0'		cm $(bkp .eth0tx) -m0 -M122 -n'eth0 tx' -u'KiB/s' -l96 -p900 -F/sys/class/net/eth0/statistics/tx_bytes -d -c1024 -C5																		#1.0
sw 1.0			cm $(bkp .eth0rx) -m0 -M122 -n'eth0 rx' -u'KiB/s' -l96 -p900 -F/sys/class/net/eth0/statistics/rx_bytes -d -c1024 -C5																		#1.1
nw 'wlan0'		cm $(bkp .wlan0tx) -m0 -M122 -n'wlan0 tx' -u'KiB/s' -l96 -p900 -F/sys/class/net/wlan0/statistics/tx_bytes -d -c1024 -C5																		#2.0
sw 2.0			cm $(bkp .wlan0rx) -m0 -M122 -n'wlan0 rx' -u'KiB/s' -l96 -p900 -F/sys/class/net/wlan0/statistics/rx_bytes -d -c1024 -C5																		#2.1
nw 'tap0'		cm $(bkp .tap0tx) -m0 -M122 -n'tap0 tx' -u'KiB/s' -l96 -p900 -F/sys/class/net/tap0/statistics/tx_bytes -d -c1024 -C5																		#3.0
sw 3.0			cm $(bkp .tap0rx) -m0 -M122 -n'tap0 rx' -u'KiB/s' -l96 -p900 -F/sys/class/net/tap0/statistics/rx_bytes -d -c1024 -C5																		#3.1

nw 'Load'		cm $(bkp .load) -a -m0 -M1 -nLoad -p900 -l96 -F/proc/loadavg -C5 -s0																														#4.0

nw 'sda'		cm $(bkp .sdar) -m0 -M10 -n'sda(r)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C5 -s2																							#5.0
sw 5.0			cm $(bkp .sdaw) -m0 -M10 -n'sda(w)' -u'MiB/s' -p900 -l96 -F/sys/block/sda/stat -d -c2048 -C5 -s6																							#5.1
nw 'sdb'		cm $(bkp .sdbr) -m0 -M10 -n'sdb(r)' -u'MiB/s' -p900 -l96 -F/sys/block/sdb/stat -d -c2048 -C5 -s2																							#6.0
sw 6.0			cm $(bkp .sdbw) -m0 -M10 -n'sdb(w)' -u'MiB/s' -p900 -l96 -F/sys/block/sdb/stat -d -c2048 -C5 -s6																							#6.1

touch marge.ipconn homer.httpcon
nw 'marge|web'	cm $(bkp .margeip) -m0 -M100 -n'Marge presence' -u% -l96 -p900 -Fmarge.ipconn -C60 -c0.01																									#7.0
sw 7.0			cm $(bkp .homerhttp) -m0 -M100 -n'Homer web connectivity' -u% -l96 -p900 -Fhomer.httpcon -C60 -c0.01																						#7.1
swb 7.0			'while true;do (ping -i1 -c3 -W1 10.8.0.2>/dev/null 2>&1 && echo 1 || echo 0)>marge.ipconn;sleep 1m;done'																					#7.2
swb 7.2			'while true;do (curl -sI -m10 --connect-timeout 10 https://www.art-software.fr/|egrep "200 OK"|wc -l)>homer.httpcon;sleep 1m;done'															#7.3

touch tel.ipconn 3ds.ipconn
nw 'tel|3ds'	cm $(bkp .telip) -m0 -M100 -n'Tel presence' -u% -l96 -p900 -Ftel.ipconn -C60 -c0.01																											#8.0
sw 8.0			cm $(bkp .3dsip) -m0 -M100 -n'3Ds presence' -u% -l96 -p900 -F3ds.ipconn -C60 -c0.01																											#8.1
swb 8.0			'while true;do (ping -c3 -i1 -W1 $(egrep "DHCPACK.*bc:41:01:13:fc:a8" /var/log/messages|egrep -o "10\.8\.0\.[0-9]+"|tail -n1)>/dev/null 2>&1 && echo 1 || echo 0)>tel.ipconn;sleep 1m;done'	#8.2
swb 8.2			'while true;do (ping -c3 -i1 -W1 $(egrep "DHCPACK.*e0:0c:7f:20:1c:6d" /var/log/messages|egrep -o "10\.8\.0\.[0-9]+"|tail -n1)>/dev/null 2>&1 && echo 1 || echo 0)>3ds.ipconn;sleep 1m;done'	#8.3

nw 'debit'		cm $(bkp .debitup) -m0 -M6 -n'Upload' -uMbit/s -l120 -p86400 -S'tail -n1 /home/adrien/.debit.homer.art-software.fr.csv|cut -d, -f2' -c1E6 -C10800											#9.0
sw 9.0			cm $(bkp .debitdown) -m0 -M40 -n'Download' -uMbit/s -l120 -p86400 -S'tail -n1 /home/adrien/.debit.homer.art-software.fr.csv|cut -d, -f3' -c1E6 -C10800										#9.1

nw 'DiskSpace'	cm $(bkp .sdaspace) -m0 -M2048 -n'SDA used' -uGiB -p21600 -l120 -S'df -k --output=used /|tail -n 1' -c1048576 -C3600																		#10.0
sw 10.0			cm $(bkp .sdbspace) -m0 -M1024 -n'SDB used' -uGiB -p21600 -l120 -S'df -k --output=used /media/videoRepoHdd/|tail -n 1' -c1048576 -C3600														#10.1

pgrep 'cm$' > /run/monitor.pid

tmux selectw -t monitor:0
